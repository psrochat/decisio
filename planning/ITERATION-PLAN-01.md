# Iteration Plan 01

## Configuration
  - [x] Docker containers / Docker compose
  - [x] Database dev environment
  - [x] Backend dev environment
  - [x] Frontend dev environment
  - [ ] CI/CD
    - [x] Build
    - [x] Test
    - [x] Release (using GCR)
    - [ ] Deploy (using Kubernetes on GKE)

## Database
  - [x] Default structure + test data insertion

## Backend
  - [x] Base API endpoints for users and subjects
  - [ ] JSON Web Tokens / Authentication

## Frontend
  - [ ] Static page (when logged out)
  - [ ] Sign Up / Sign In (email/username, password)
  - [x] Dashboard
    - [x] List of open/closed subjects (tags)
  - [ ] Create new subject on which a decision has to be made
    - [ ] Groups creation (invite other users to the subject)
