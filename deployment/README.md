### Set up helm:

- Helm is a package manager for Kubernetes (like npm for Node).
  To install Helm :

  ```
  curl -o get_helm.sh https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get
  chmod +x get_helm.sh
  ./get_helm.sh
  ```

- Deploy `ServiceAccount` and `ClusterRoleBinding` to grant the necessary role
  and a permissions to Tiller (the Helm server-side component).   
  See tiller-sa-rbac.yml in /configure directory   
  Ref: https://docs.helm.sh/using_helm/#role-based-access-control

  ```
  kubectl apply -f tiller-sa-rbac.yml
  ```

- Then init Helm:
  ```
  helm init --client-only
  ```
  It will set up any necessary local configuration.

- Create `ServiceAccount` and Kubernetes `Secret`
  ```
  NAME=clouddns-service-account
  PROJECT=pdg-decisio
  KEY=cert-manager-key.json

  gcloud iam service-accounts create ${NAME} --display-name=${NAME} --project=${PROJECT}
  gcloud iam service-accounts keys create ./${KEY} --iam-account=${NAME}@${PROJECT}.iam.gserviceaccount.com --project=${PROJECT}
  gcloud projects add-iam-policy-binding ${PROJECT} --member=serviceAccount:${NAME}@${PROJECT}.iam.gserviceaccount.com --role=roles/dns.admin
  kubectl create secret generic ${NAME} --from-file=./${KEY} --namespace=cert-manager
  ```

- Install `cert-manager`

  ```
  # Install
  helm install --name cert-manager --namespace cert-manager stable/cert-manager --set ingressShim.defaultIssuerName=letsencrypt --set ingressShim.defaultIssuerKind=ClusterIssuer

  # Upgrade
  helm upgrade cert-manager stable/cert-manager

  # Delete
  helm del --purge cert-manager
  kubectl get customresourcedefinition
  kubectl delete customresourcedefinition certificates.certmanager.k8s.io clusterissuers.certmanager.k8s.io issuers.certmanager.k8s.io
  ```

- Deploy a `ClusterIssuer`, this represent a certificate authority from which
  signed x509 certificates can be obtained, such as Let’s Encrypt, or your own
  signing key pair stored in a Kubernetes Secret resource.
  ```
  kubectl apply -f cert-manager.yml
  ```

- Deploy `Certificate`
  ```
  kubectl apply -f certificate.yml
  ```

- Install `nginx-ingress` controller + default backend
  ```
  # Install
  helm install --name nginx-ingress stable/nginx-ingress --set rbac.create=true

  # Upgrade
  helm upgrade stable/nginx-ingress --name nginx-ingress

  # Delete
  helm del --purge nginx-ingress
  ```

- Deploy `nginx-ingress` that contains the routing rules.
  ```
  kubectl apply -f ingress.yml
  ```
