#!/bin/sh

# Exit script if a command fails
set -e

# Wait for services to be up and running
./wait-for-it.sh web:80 --timeout=30 --strict -- echo "nginx is up"
./wait-for-it.sh node:8081 --timeout=30 --strict -- echo "node is up"
./wait-for-it.sh mongodb:27017 --timeout=30 --strict -- echo "mongodb is up"

# Run tests
npm run test
