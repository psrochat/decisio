# Nginx Node MongoDB

Docker running Nginx, Node and MongoDB.

## Overview

1. [Install prerequisites](#install-prerequisites)

    Before installing project make sure the following prerequisites have been met.

2. [Clone the project](#clone-the-project)

    We’ll download the code from its repository on GitHub.

3. [Run the application](#run-the-application)

    By this point we’ll have all the project pieces in place.
___

## Install prerequisites

For now, this project has been mainly created for Unix `(Linux/MacOS)`. Perhaps it could work on Windows.

All requisites should be available for your distribution. The most important are :

* [Git](https://git-scm.com/downloads)
* [Docker](https://docs.docker.com/engine/installation/)
* [Docker Compose](https://docs.docker.com/compose/install/)

Check if `docker-compose` is already installed by entering the following command :

```sh
which docker-compose
```

Check Docker Compose compatibility :

* [Compose file version 3 reference](https://docs.docker.com/compose/compose-file/)

### Images

The images used for the deployment are based on:

* [Nginx](https://hub.docker.com/_/nginx/)
* [Node](https://hub.docker.com/_/node/)
* [MongoDB](https://hub.docker.com/r/bitnami/mongodb/)

The following ports are used by the deployment:

| Server     | Port  |
|------------|-------|
| MongoDB    | 27017 |
| Nginx      | 8000  |
| Node       | 8081  |
___

## Clone the project

```sh
git clone https://gitlab.com/psrochat/decisio.git
```
or using SSH:
```sh
git clone git@gitlab.com:psrochat/decisio.git
```

Go to the project directory :

```sh
cd decisio
```

### Project tree

```sh
.
├── README.md
├─── planning/
│    ├── ITERATION-PLAN-01.md
│    ├── ITERATION-PLAN-02.md
│    └── ITERATION-PLAN-03.md
├─── stack/
│    ├─── deployment/
│    │    └── ...
│    │─── staging/
|    |    └── ...
│    ├─── data/
│    │    └── db/
│    │        ├── mongodb/
│    │        └── Dockerfile
│    ├─── backend/
│    │    ├── models/
│    │    │   └── ...
│    │    ├── src/
│    │    │   ├── server.js
│    │    │   └── ...
│    │    └── Dockerfile
│    ├─── frontend-web/
│    │    └── react-app/
│    │        ├── public/
│    │        │   └── index.html
│    │        ├── src/
│    │        │   └── ...
│    │        └── ...
│    ├─── web/
│    │    ├── public/
│    │    │   ├── static/
│    │    │   │   └── ...
│    │    │   └── ...
│    │    ├── Dockerfile
│    │    └── nginx.conf
│    └── ...
├── .gitlab-ci.yml
└── ...
```

___

## Run the application locally

1. Start the containers :

    ```sh
    ./start-dev.sh
    ```

    **Please wait, this might take a few minutes...**

    ```sh
    docker-compose logs -f # Follow log output
    ```

2. Start the application:

    ```sh
    cd ./frontend-web/react-app
    npm start
    ```

2. Open your favorite browser :

    * [http://localhost:8000](http://localhost:8000/)
    * [https://localhost:3000](https://localhost:3000/)
    * [http://localhost:27017](http://localhost:27017/) MongoDB (username: dev, password: dev)

3. Stop and clear services

    ```sh
    docker-compose down -v
    ```

___
