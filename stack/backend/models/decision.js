const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const Schema = mongoose.Schema

const option = new Schema({
  title: String,
  description: String,
  votes: Number
})

const decision = new Schema({
  title: String,
  description: String,
  author: { type: Schema.Types.ObjectId, ref: 'collaborators' },
  views: Number,
  lastUpdate: Date,
  date: Date,
  state: {
    type: String,
    enum: ['Open', 'Closed']
  },
  teams: [{ type: Schema.Types.ObjectId, ref: 'teams' }],
  options: [option],
  tags: [{ type: Schema.Types.ObjectId, ref: 'tags' }]
})

decision.plugin(mongoosePaginate)

module.exports = mongoose.model('decisions', decision)
