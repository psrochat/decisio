const mongoose = require('mongoose')

const Schema = mongoose.Schema

const collaborator = new Schema({
  name: String,
  birthday: Date,
  organization: { type: Schema.Types.ObjectId, ref: 'organizations' },
  position: String,
  profileImage: { data: Buffer, contentType: String },
  username: String,
  password: String,
  email: String,
  isAdmin: Boolean,
  firebaseId: String
})

module.exports = mongoose.model('collaborators', collaborator)
