const mongoose = require('mongoose')

const Schema = mongoose.Schema

const organization = new Schema({
  name: String
})

module.exports = mongoose.model('organizations', organization)
