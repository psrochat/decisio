const mongoose = require('mongoose')
const findOneOrCreate = require('mongoose-findoneorcreate')

const Schema = mongoose.Schema

const userContribVote = new Schema({
  user: { type: Schema.Types.ObjectId, ref: 'collaborators' },
  contribution: { type: Schema.Types.ObjectId, ref: 'contributions' },
  score: Number
})

userContribVote.plugin(findOneOrCreate)

module.exports = mongoose.model('userContribVotes', userContribVote)
