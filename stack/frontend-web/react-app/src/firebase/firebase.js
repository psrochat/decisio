import firebase from 'firebase/app'
import 'firebase/auth'

const prodConfig = {
  apiKey: 'AIzaSyClZHOxHx9o-6wIxNGLvD8P-gneOUoZQ_s',
  authDomain: 'decisio-faf14.firebaseapp.com',
  databaseURL: 'https://decisio-faf14.firebaseio.com',
  projectId: 'decisio-faf14',
  storageBucket: 'decisio-faf14.appspot.com',
  messagingSenderId: '1096904166371'
}

const devConfig = {
  apiKey: 'AIzaSyDS3i3SmFEQtY-KkZeHI0eC3Ad7F9nvVhc',
  authDomain: 'decisio-dev.firebaseapp.com',
  databaseURL: 'https://decisio-dev.firebaseio.com',
  projectId: 'decisio-dev',
  storageBucket: 'decisio-dev.appspot.com',
  messagingSenderId: '858804861546'
}

const config = process.env.REACT_APP_ENV === 'production' ? prodConfig : devConfig

if (!firebase.apps.length) {
  firebase.initializeApp(config)
}

const auth = firebase.auth()

export default auth
