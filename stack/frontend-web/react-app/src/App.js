import React from 'react'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faAngleDown, faTimesCircle, faPlus, faCheck, fas } from '@fortawesome/free-solid-svg-icons'
import Main from './components/Main'
import Header from './components/header/Header'
import withAuthentication from './components/withAuthentication'
import withLoading from './components/withLoading'

library.add(faAngleDown, faTimesCircle, faPlus, faCheck, fas)

const App = () => (
  <div>
    <Header />
    <Main />
  </div>
)

export default withAuthentication(withLoading(App))
