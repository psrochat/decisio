import React from 'react'
import PropTypes from 'prop-types'
import { Container, ListGroup, Badge, ListGroupItem } from 'reactstrap'
import * as routes from '../../constants/routes'

const Team = (props) => {
  const { team } = props

  let badgeColor = 'success'

  return (
    <Container>
      <h3>{team.name}</h3>
      <ListGroup>

        { team.participants.map((participant) => {
          if (participant.role === 'Contributor') {
            badgeColor = 'primary'
          } else if (participant.role === 'Concerned') {
            badgeColor = 'secondary'
          }
          return (
            <ListGroupItem tag="a" href={`${routes.PROFILE}/${participant.collaborator._id}`} key={participant.collaborator._id} className="justify-content-between">
              {participant.collaborator.name}
              <Badge className="float-right" color={badgeColor}>{participant.role}</Badge>
            </ListGroupItem>
          )
        }) }
      </ListGroup>
    </Container>
  )
}

Team.propTypes = {
  team: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    organization: PropTypes.shape({
      _id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired
    }).isRequired,
    participants: PropTypes.array.isRequired
  }).isRequired
}

export default Team
