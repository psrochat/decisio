import React from 'react'
import PropTypes from 'prop-types'
import { Container } from 'reactstrap'
import axios from '../../axios'
import withAuthorization from '../withAuthorization'
import Team from './Team'

class Decisions extends React.Component {
  static propTypes = {
    collaboratorId: PropTypes.string.isRequired,
    idToken: PropTypes.string.isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      error: null,
      hasLoaded: false,
      teams: []
    }
  }

  fetchTeams = () => axios.get(`/teams/user/${this.props.collaboratorId}`, { headers: { Authorization: `Bearer ${this.props.idToken}` } })
    .then((response) => {
      this.setState({
        teams: response.data
      })
    })
    .then(() => {
      this.setState({
        hasLoaded: true
      })
    })
    .catch(error => this.setState({
      error
    }))

  componentDidMount = () => {
    this.fetchTeams()
  }

  render() {
    const { error, hasLoaded, teams } = this.state
    if (error) {
      return (
        <div>Error: {error.message}</div>
      )
    }
    if (hasLoaded) {
      return (
        <Container>
          <div>{ teams.map(team => (
            <Team
              team={team}
              key={team._id}
            />
          )) }
          </div>
        </Container>
      )
    }
    return (
      <div>Loading...</div>
    )
  }
}

const authCondition = authUser => !!authUser

export default withAuthorization(authCondition)(Decisions)
