import React from 'react'
import PropTypes from 'prop-types'
import { Container } from 'reactstrap'
import axios from '../../axios'
import withAuthorization from '../withAuthorization'
import DecisionSubject from '../decision/DecisionSubject'
import Contributions from '../decision/Contributions'

import '../../assets/scss/Decision.scss'

class Decision extends React.Component {
  static propTypes = {
    idToken: PropTypes.string.isRequired,
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.string.isRequired
      }).isRequired
    }).isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      error: null,
      hasLoaded: false,
      decision: null
    }
  }

  componentDidMount() {
    this.fetchDecision()
      .then(() => this.computeTotalVotes())
      .then(() => this.setState({
        hasLoaded: true
      }))
  }

  fetchDecision = () => {
    const { id } = this.props.match.params
    return axios.get(`/decision/${id}`, { headers: { Authorization: `Bearer ${this.props.idToken}` } })
      .then((response) => {
        this.setState({
          decision: response.data
        })
      })
      .catch(error => this.setState({
        error
      }))
  }

  computeTotalVotes = () => {
    const { options } = this.state.decision
    options.forEach((option) => {
      this.setState(prevState => ({
        totalVotes: prevState.totalVotes + option.votes
      }))
    })
  }

  render() {
    const { error, hasLoaded, decision } = this.state
    if (error) {
      return (
        <div>Error: {error.message}</div>
      )
    }
    if (hasLoaded) {
      return (
        <Container className="mt-4">
          <DecisionSubject decision={decision} />
          <div className="decision-contributions-section">
            <Contributions decisionId={decision._id} idToken={this.props.idToken} />
          </div>
        </Container>
      )
    }
    return (<div>Loading...</div>)
  }
}

const authCondition = authUser => !!authUser

export default withAuthorization(authCondition)(Decision)
