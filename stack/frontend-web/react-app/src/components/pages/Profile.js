import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Container, Button, Row, Col, Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap'
import classnames from 'classnames'
import axios from '../../axios'
import * as routes from '../../constants/routes'
import withAuthorization from '../withAuthorization'
import UserContributions from '../profile/UserContributions'
import Teams from '../profile/Teams'
import userImage from '../../assets/images/user.png'

class Profile extends React.Component {
  static propTypes = {
    idToken: PropTypes.string.isRequired,
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.string.isRequired
      }).isRequired
    }).isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      error: false,
      hasLoaded: false,
      activeTab: 'Infos',
      collaborator: null,
      profileImage: userImage
    }
  }

  fetchCollaborator = () => axios.get(`/collaborators/${this.props.match.params.id}`, { headers: { Authorization: `Bearer ${this.props.idToken}` } })
    .then((response) => {
      this.setState({
        collaborator: response.data
      })
    })
    .then(() => {
      this.setState({
        hasLoaded: true
      })
    })
    .catch(error => this.setState({
      error
    }))

  componentDidMount = () => {
    this.fetchCollaborator()
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      })
    }
  }

  render() {
    const { error, hasLoaded, collaborator, profileImage } = this.state
    if (error) {
      return (
        <div>Error: {error.message}</div>
      )
    }
    if (hasLoaded) {
      return (
        <Container className="mt-4">
          <h1>Profile</h1>
          <hr />
          <Row>
            <Col>
              <Nav tabs>
                <NavItem>
                  <NavLink
                    id="infos"
                    className={classnames({ active: this.state.activeTab === 'Infos' })}
                    onClick={() => { this.toggle('Infos') }}
                  >
                    Profile informations
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    id="contributions"
                    className={classnames({ active: this.state.activeTab === 'Contributions' })}
                    onClick={() => { this.toggle('Contributions') }}
                  >
                    Contributions
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    id="teams"
                    className={classnames({ active: this.state.activeTab === 'Teams' })}
                    onClick={() => { this.toggle('Teams') }}
                  >
                    Teams
                  </NavLink>
                </NavItem>
              </Nav>
            </Col>
          </Row>
          <TabContent style={{ width: '100%' }} activeTab={this.state.activeTab}>
            <TabPane tabId="Infos">
              <Row>
                <Col xs="auto">
                  <div style={{ marginTop: '15px', width: '200px', float: 'right' }}>
                    <p><img src={profileImage} width="200px" className="navbar-default" alt="logo" /></p>
                  </div>
                </Col>
                <Col>
                  <div style={{ marginTop: '15px' }}>
                    <h3>Name</h3>
                    <p>{collaborator.name}</p>
                    <h3>Works at</h3>
                    <p>{collaborator.organization.name} as {collaborator.position}</p>
                  </div>
                </Col>
                <Col>
                  <div style={{ marginTop: '15px' }}>
                    <h3>Email</h3>
                    <p>{collaborator.email}</p>
                    <h3>Birthday</h3>
                    <p>{collaborator.birthday}</p>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col>
                  <div style={{ float: 'right' }}>
                    <Button
                      color="primary"
                      className="action-btn"
                      to={routes.EDITPROFILE}
                      tag={Link}
                    >
                      Edit profile
                    </Button>
                  </div>
                </Col>
              </Row>
            </TabPane>
          </TabContent>
          <TabContent style={{ width: '100%' }} activeTab={this.state.activeTab}>
            <TabPane tabId="Contributions">
              <UserContributions idToken={this.props.idToken} userId={collaborator._id} />
            </TabPane>
          </TabContent>
          <TabContent style={{ width: '100%' }} activeTab={this.state.activeTab}>
            <TabPane tabId="Teams">
              <Teams idToken={this.props.idToken} collaboratorId={collaborator._id} />
            </TabPane>
          </TabContent>
        </Container>
      )
    }
    return (
      <div>Loading...</div>
    )
  }
}

const authCondition = authUser => !!authUser

export default withAuthorization(authCondition)(Profile)
