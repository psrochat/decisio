import React from 'react'
import { Container } from 'reactstrap'
import '../../assets/scss/Home.scss'

const Home = () => (
  <Container className="mt-4">
    <h1>Home</h1>
    <hr />
  </Container>
)

export default Home
