import React from 'react'
import { Container, Button, Row, Col, Form, FormGroup, Label, Input, Alert } from 'reactstrap'
import PropTypes from 'prop-types'
import DatePicker from 'react-datepicker'
import userImage from '../../assets/images/user.png'
import withAuthorization from '../withAuthorization'
import axios from '../../axios'
import '../../assets/scss/Decision.scss'
import 'react-datepicker/dist/react-datepicker.css'

class EditProfile extends React.Component {
  static propTypes = {
    idToken: PropTypes.string.isRequired,
    collaborator: PropTypes.shape({
      _id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      birthday: PropTypes.string.isRequired,
      organization: PropTypes.shape({
        _id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired
      }).isRequired,
      position: PropTypes.string.isRequired,
      username: PropTypes.string.isRequired,
      password: PropTypes.string.isRequired,
      email: PropTypes.string.isRequired,
      isAdmin: PropTypes.bool.isRequired,
      firebaseId: PropTypes.string.isRequired
    }).isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      error: false,
      collaborator: this.props.collaborator,
      selectedFile: null,
      updateStatus: null,
      profileImage: userImage
    }
  }

  onSubmit = (event) => {
    event.preventDefault()

    axios.put('/collaborator', this.state.collaborator, { headers: { Authorization: `Bearer ${this.props.idToken}` } })
      .then((result) => {
        if (result.status === 200) {
          this.setState({
            updateStatus: { status: 'success', message: 'Successfully updated' }
          })
        } else {
          this.setState({
            updateStatus: { status: 'danger', message: 'Something went wrong' }
          })
        }
      })
  }

  handleUpload = () => {
    const data = new FormData()
    data.append('profileImage', this.state.selectedFile, this.state.selectedFile.name)

    axios
      .post('/profileimage', data, { headers: { Authorization: `Bearer ${this.props.idToken}` } })
      .then((collaborator) => {
        this.setState({
          collaborator
        })
      })
  }

  handleselectedFile = (event) => {
    this.setState({
      selectedFile: event.target.files[0]
    }, this.handleUpload)
  }

  handleChange(field, value) {
    this.setState(prevState => ({
      collaborator: { ...prevState.collaborator, [field]: value }
    }))
  }

  fetchProfileImage() {
    axios.get(
      '/profileimage',
      { responseType: 'arraybuffer' },
      { headers: { Authorization: `Bearer ${this.props.idToken}` } }
    )
      .then((result) => {
        const base64 = btoa(
          new Uint8Array(result.data).reduce(
            (imageData, byte) => imageData + String.fromCharCode(byte),
            '',
          ),
        )
        this.setState({ profileImage: `data:;base64,${base64}` })
      })
  }

  render() {
    const { error, collaborator, updateStatus } = this.state
    if (error) {
      return (
        <div>Error: {error.message}</div>
      )
    }
    return (
      <Container>
        <h1>Edit profile</h1>
        <hr />
        <Form onSubmit={this.onSubmit}>
          <Row>
            <Col xs="auto">
              <div style={{ marginTop: '15px', width: '200px', float: 'right' }}>
                <p><img src={this.state.profileImage} width="200px" className="navbar-default" alt="logo" /></p>
                <Input type="button" value="Upload new image" accept="image/*" onChange={this.handleselectedFile} />
              </div>
            </Col>
            <Col>
              <div style={{ marginTop: '15px' }}>
                <FormGroup>
                  <Label className="d-inline" for="name">Name</Label>
                  <Input
                    id="name"
                    value={collaborator.name}
                    onChange={event => this.handleChange('name', event.target.value)}
                    type="text"
                    placeholder="Name"
                    className="sign-form"
                  />
                </FormGroup>
                <FormGroup>
                  Birthday<br />
                  <DatePicker
                    id="birthday"
                    selected={new Date(collaborator.birthday)}
                    onChange={date => this.handleChange('birthday', date)}
                  />
                </FormGroup>
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <div style={{ float: 'right' }}>
                <Button color="primary" type="submit" className="action-btn">
                  Save changes
                </Button>
              </div>
            </Col>
          </Row>
          <Row>
            <div style={{ marginTop: '15px', width: '100%', float: 'right', textAlign: 'center' }}>
              {updateStatus ? (
                <Alert color={updateStatus.status}>
                  {updateStatus.message}
                </Alert>
              ) : <div />
              }
            </div>
          </Row>
        </Form>
      </Container>
    )
  }
}

const authCondition = authUser => !!authUser

export default withAuthorization(authCondition)(EditProfile)
