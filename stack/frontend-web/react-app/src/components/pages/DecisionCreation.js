import React from 'react'
import PropTypes from 'prop-types'
import { Container, Form, FormGroup, Label, Input, DropdownToggle, DropdownMenu, DropdownItem, UncontrolledDropdown, Badge, Button, Nav, NavLink, NavItem, TabContent, TabPane } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import classnames from 'classnames'
import * as routes from '../../constants/routes'
import '../../assets/scss/DecisionCreation.scss'
import Spinner from '../utils/Spinner'
import axios from '../../axios'
import withAuthorization from '../withAuthorization'

class DecisionCreation extends React.Component {
  static propTypes = {
    userId: PropTypes.string.isRequired,
    idToken: PropTypes.string.isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired
    }).isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      error: null,
      hasLoaded: false,
      decisionTitle: '',
      decisionDescription: '',
      options: [],
      optionId: 2,
      dropdownOpen: false,
      teamsInput: '',
      teamTags: [],
      highlightedTeam: -1,
      selectedTeamTags: [],
      activeTab: 'Option 1'
    }
  }

  componentDidMount = () => {
    this.fetchTeams()
      .then(() => this.setState({
        options: [{
          id: 1,
          title: 'Option 1',
          description: ''
        }, {
          id: 2,
          title: 'Option 2',
          description: ''
        }]
      }))
  }

  fetchTeams = () => axios.get(`/teams/user/${this.props.userId}`, { headers: { Authorization: `Bearer ${this.props.idToken}` } })
    .then((response) => {
      this.setState({
        teamTags: response.data,
        hasLoaded: true
      })
    })
    .catch(error => this.setState({
      error
    }))

  handleTitleChange = (e) => {
    this.setState({
      decisionTitle: e.target.value
    })
  }

  handleDescriptionChange = (e) => {
    this.setState({
      decisionDescription: e.target.value
    })
  }

  addOption = () => {
    this.setState(prevState => ({
      optionId: prevState.optionId + 1,
      options: [...prevState.options, {
        id: prevState.optionId + 1,
        title: 'New option',
        description: ''
      }]
    }))
  }

  removeOption = (index) => {
    this.setState(prevState => ({
      options: prevState.options.filter((el, i) => i !== index)
    }))
  }

  displayClearButton = (index) => {
    if (this.state.options.length > 2) {
      return (<FontAwesomeIcon icon={['fas', 'times']} className="remove-button" onClick={() => this.removeOption(index)} />)
    }
    return null
  }

  handleOptionTitleChange = (e, index) => {
    e.persist()
    this.setState((prevState) => {
      const options = prevState.options.slice()
      options[index].title = e.target.value
      return {
        options
      }
    })
  }

  handleOptionDescriptionChange = (e, index) => {
    e.persist()
    this.setState((prevState) => {
      const options = prevState.options.slice()
      options[index].description = e.target.value
      return {
        options
      }
    })
  }

  teamsChange = (e) => {
    const { value } = e.target
    this.setState(prevState => ({
      teamsInput: value,
      dropdownOpen: prevState.teamTags.filter(team => new RegExp(`${value}.*`, 'i').test(team.name)).length > 0
    }))
  }

  teamSelected = (team) => {
    this.setState(prevState => ({
      teamTags: prevState.teamTags.filter(el => el._id !== team._id),
      selectedTeamTags: [...prevState.selectedTeamTags, team],
      dropdownOpen: false,
      teamsInput: ''
    }))
  }

  closeTeamTag = (team) => {
    this.setState(prevState => ({
      selectedTeamTags: prevState.selectedTeamTags.filter(el => el._id !== team._id),
      teamTags: [...prevState.teamTags, team]
    }))
  }

  toggle = () => {
    this.setState({
      dropdownOpen: this.getFilteredTeams().length > 0
    })
  }

  highlightTeam = (e) => {
    switch (e.keyCode) {
      // arrow up
      case 38:
        this.setState(prevState => ({
          highlightedTeam: prevState.highlightedTeam - 1 <= -1 ?
            this.getFilteredTeams().length - 1 : prevState.highlightedTeam - 1,
          dropdownOpen: true
        }))
        break
      // arrow down
      case 40:
        this.setState(prevState => ({
          highlightedTeam: (prevState.highlightedTeam + 1) % this.getFilteredTeams().length,
          dropdownOpen: true
        }))
        break
        // enter
      case 13:
        if (this.state.highlightedTeam > -1) {
          this.teamSelected(this.getFilteredTeams()[this.state.highlightedTeam])
        }
        break
      default:
        this.teamsChange(e)
    }
  }

  teamIsHighlighted = index => index === this.state.highlightedTeam

  getFilteredTeams = () => {
    const { teamsInput, teamTags } = this.state
    return teamTags.filter(team => new RegExp(`${teamsInput}.*`, 'i').test(team.name)).sort((a, b) => a.name.localeCompare(b.name))
  }


  submit = () => {
    const { decisionTitle, decisionDescription, options, selectedTeamTags } = this.state
    const { userId } = this.props
    axios.post('/decisions', {
      userId: this.props.userId,
      title: decisionTitle,
      description: decisionDescription,
      author: userId,
      teams: selectedTeamTags.map(team => team._id),
      options
    }, {
      headers: { Authorization: `Bearer ${this.props.idToken}` }
    })
      .then((response) => {
        this.props.history.push(`${routes.DECISION}/${response.data._id}`)
      })
      .catch((error) => {
        this.setState({
          error
        })
      })
  }

  toggleTab(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      })
    }
  }

  render() {
    const { error, hasLoaded } = this.state
    if (error) {
      return (
        <div>Error: {error.message}</div>
      )
    }
    if (hasLoaded) {
      const { options, selectedTeamTags } = this.state
      const filteredTeams = this.getFilteredTeams()
      return (
        <div style={{ marginTop: '15px' }}>
          <Container>
            <Form>
              <FormGroup>
                <Label for="decisionTitle">Title</Label>
                <Input type="text" name="decisionTitle" id="decisionTitle" placeholder="Enter the title of your decision" onChange={this.handleTitleChange} />
              </FormGroup>
              <FormGroup>
                <Label for="decisionDescription">Decision description</Label>
                <Input type="textarea" name="decisionDescription" id="decisionDescription" onChange={this.handleDescriptionChange} />
              </FormGroup>
              <Label>Options</Label>
              <Nav tabs>
                {
                  options.map((option, index) => (
                    <NavItem key={option.id}>
                      <NavLink
                        id={`Option ${index + 1}`}
                        className={classnames({ active: this.state.activeTab === `Option ${index + 1}` })}
                        onClick={() => { this.toggleTab(`Option ${index + 1}`) }}
                      >
                        {option.title} { this.displayClearButton(index) }
                      </NavLink>
                    </NavItem>
                  ))
                }
                <NavItem>
                  <NavLink
                    onClick={() => { this.addOption() }}
                  >
                    <FontAwesomeIcon icon="plus" className="add-button" />
                  </NavLink>
                </NavItem>
              </Nav>
              {
                options.map((option, index) => (
                  <TabContent key={option.id} style={{ width: '100%', marginTop: '15px' }} activeTab={this.state.activeTab}>
                    <TabPane tabId={`Option ${index + 1}`}>
                      <FormGroup key={option.id}>
                        <Label for={`option${index}`}>Title</Label>
                        <Input type="text" name={`option${index}-title`} id={`option${index}-title`} onChange={e => this.handleOptionTitleChange(e, index)} />
                        <Label for={`option${index}`}>Description</Label>
                        <Input type="textarea" name={`option${index}-description`} id={`option${index}-description`} onChange={e => this.handleOptionDescriptionChange(e, index)} />
                      </FormGroup>
                    </TabPane>
                  </TabContent>
                ))
              }
              <FormGroup>
                <div>
                  {
                    selectedTeamTags.map(team => (
                      <Badge key={team._id} color="dark">{team.name} <Button close onClick={() => this.closeTeamTag(team)} /></Badge>
                    ))
                  }
                </div>
                <UncontrolledDropdown isOpen={this.state.dropdownOpen}>
                  <DropdownToggle tag="div">
                    <Input
                      placeholder="Search teams.."
                      id="teamSearch"
                      type="text"
                      value={this.state.teamsInput}
                      onChange={this.teamsChange}
                      onClick={this.toggle}
                      onKeyDown={this.highlightTeam}
                    />
                  </DropdownToggle>
                  <DropdownMenu
                    modifiers={{
                      setMaxHeight: {
                        enabled: true,
                        order: 890,
                        fn: data => ({
                          ...data,
                          styles: {
                            ...data.styles,
                            overflow: 'auto',
                            maxHeight: 100
                          }
                        })
                      }
                    }}
                  >
                    {
                      filteredTeams.map((team, index) => (
                        <DropdownItem
                          active={this.teamIsHighlighted(index)}
                          key={team._id}
                          onClick={() => this.teamSelected(team)}
                        >
                          {team.name}
                        </DropdownItem>
                      ))
                    }
                  </DropdownMenu>
                </UncontrolledDropdown>
              </FormGroup>
              <Button color="success" onClick={this.submit}>Add decision</Button>
            </Form>
          </Container>
        </div>
      )
    }
    return (<Spinner />)
  }
}

const authCondition = authUser => !!authUser

export default withAuthorization(authCondition)(DecisionCreation)
