import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { Container, Button, Form, FormGroup, Label, Input, Alert } from 'reactstrap'
import axios from '../../axios'
import { auth } from '../../firebase'
import * as routes from '../../constants/routes'

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value
})

const INITIAL_STATE = {
  name: '',
  email: '',
  passwordOne: '',
  passwordTwo: '',
  error: null,
  verticalPos: 0,
  mainHeight: 0
}

class SignUpPage extends Component {
  static propTypes = {
    history: PropTypes.shape({
      push: PropTypes.func
    }).isRequired
  }

  constructor(props) {
    super(props)
    this.state = { ...INITIAL_STATE }
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions)
    this.updateDimensions()
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions)
    document.body.style.overflow = 'scroll'
  }

  updateDimensions = () => {
    const headerHeight = 80
    const windowHeight = window.innerHeight
    const containerHeight = this.container.clientHeight

    // Set container vertical position and main container height
    const verticalPos = Math.max((windowHeight / 2) - (containerHeight / 2), headerHeight + 50)
    const mainHeight = Math.max(window.innerHeight - headerHeight, containerHeight + 100)
    // Enable or disable scroll
    if (verticalPos > headerHeight + 50) {
      document.body.style.overflow = 'hidden'
    } else {
      document.body.style.overflow = 'scroll'
    }

    // State
    this.setState({ verticalPos, mainHeight })
  }

  onSubmit = (event) => {
    const {
      name,
      email,
      passwordOne
    } = this.state

    const {
      history
    } = this.props

    auth.doCreateUserWithEmailAndPassword(email, passwordOne)
      .then((user) => {
        axios.post('/collaborators', {
          name,
          email,
          firebaseId: user.authUser.uid
        })
          .then((response) => { // eslint-disable-line no-unused-vars
            this.setState({ ...INITIAL_STATE })
            history.push(routes.BOARD)
          })
          .catch((error) => {
            this.setState({ error })
          })
      })
      .catch((error) => {
        this.setState(byPropKey('error', error))
      })

    event.preventDefault()
  }

  render() {
    const {
      name,
      email,
      passwordOne,
      passwordTwo,
      error,
      verticalPos,
      mainHeight
    } = this.state

    return (
      <div
        className="main-dark"
        style={{ minHeight: `${mainHeight}px` }}
      >
        <Container>
          <div
            ref={(elem) => { this.container = elem }}
            className="sign-form-container justify-content-center"
            style={{ top: `${verticalPos}px` }}
          >
            <h1 className="text-center">Sign Up</h1>
            <Form onSubmit={this.onSubmit}>
              { error && <Alert color="dark-danger" className="mt-3">{error.message}</Alert> }
              <FormGroup>
                <Label for="signUpEmail">FULL NAME</Label>
                <Input
                  id="signUpName"
                  value={name}
                  onChange={event => this.setState(byPropKey('name', event.target.value))}
                  type="text"
                  placeholder="Full Name"
                  className="sign-form"
                />
              </FormGroup>
              <FormGroup>
                <Label for="signUpEmail">EMAIL</Label>
                <Input
                  id="signUpEmail"
                  value={email}
                  onChange={event => this.setState(byPropKey('email', event.target.value))}
                  type="email"
                  placeholder="Email Address"
                  className="sign-form"
                />
              </FormGroup>
              <FormGroup>
                <Label for="signUpPasswordOne">PASSWORD</Label>
                <Input
                  id="signUpPasswordOne"
                  value={passwordOne}
                  onChange={event => this.setState(byPropKey('passwordOne', event.target.value))}
                  type="password"
                  placeholder="Password"
                  className="sign-form"
                />
              </FormGroup>
              <FormGroup>
                <Label for="signUpPasswordTwo">CONFIRM PASSWORD</Label>
                <Input
                  id="signUpPasswordTwo"
                  value={passwordTwo}
                  onChange={event => this.setState(byPropKey('passwordTwo', event.target.value))}
                  type="password"
                  placeholder="Password"
                  className="sign-form"
                />
              </FormGroup>
              <Button color="brand" type="submit" className="action-btn">
                Sign Up
              </Button>
            </Form>
          </div>
        </Container>
      </div>
    )
  }
}

export default withRouter(SignUpPage)
