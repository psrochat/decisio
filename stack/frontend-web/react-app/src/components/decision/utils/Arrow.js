import React from 'react'
import PropTypes from 'prop-types'

const Arrow = props => (
  <svg
    id="Arrow"
    data-name="Arrow"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 118 77"
    className={props.classes}
  >
    <title>Arrow</title>
    {
      props.downvote ?
        (<path d="M55.1,75.1,1.5,8.1A5,5,0,0,1,5.4,0H112.6a5,5,0,0,1,3.9,8.1l-53.6,67A5,5,0,0,1,55.1,75.1Z" />) :
        (<path d="M62.9,1.88l53.6,67a5,5,0,0,1-3.9,8.1H5.4a5,5,0,0,1-3.9-8.1l53.6-67A5,5,0,0,1,62.9,1.88Z" />)
    }
  </svg>
)

Arrow.propTypes = {
  classes: PropTypes.string.isRequired,
  downvote: PropTypes.bool
}

Arrow.defaultProps = {
  downvote: false
}

export default Arrow
