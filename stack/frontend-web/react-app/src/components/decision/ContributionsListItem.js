import React from 'react'
import { Link } from 'react-router-dom'
import { Button, Alert } from 'reactstrap'
import PropTypes from 'prop-types'
import TimeAgo from 'react-timeago'
import axios from '../../axios'
import * as routes from '../../constants/routes'
import Comments from './Comments'
import Spinner from '../utils/Spinner'
import Arrow from './utils/Arrow'
import user from '../../assets/images/user.png'

class ContributionsListItem extends React.Component {
  static propTypes = {
    contribution: PropTypes.shape({
      _id: PropTypes.string.isRequired,
      author: PropTypes.shape({
        name: PropTypes.string.isRequired,
        _id: PropTypes.string.isRequired
      }).isRequired,
      option: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
      date: PropTypes.string.isRequired,
      points: PropTypes.number.isRequired,
      comments: PropTypes.arrayOf(
        PropTypes.shape({
          author: PropTypes.shape({
            name: PropTypes.string.isRequired,
            _id: PropTypes.string.isRequired
          }).isRequired,
          text: PropTypes.string.isRequired,
          date: PropTypes.string.isRequired,
          points: PropTypes.number.isRequired
        }).isRequired
      ).isRequired
    }).isRequired,
    idToken: PropTypes.string.isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      error: null,
      hasLoaded: false,
      contribution: props.contribution,
      upvoteState: 0
    }
  }

  componentDidMount() {
    this.fetchUpvoteState()
  }

  updateScore = (value) => {
    axios.put(`/contribution/${this.state.contribution._id}/updatescore`, { value }, { headers: { Authorization: `Bearer ${this.props.idToken}` } })
      .then((response) => {
        this.setState(prevState => ({
          contribution: {
            ...prevState.contribution,
            points: prevState.contribution.points + response.data.increment
          },
          upvoteState: response.data.score,
          hasLoaded: true
        }))
      })
      .catch(error => this.setState({
        error
      }))
  }

  fetchUpvoteState = () => {
    axios.get(`/uservote/contribution/${this.state.contribution._id}`, { headers: { Authorization: `Bearer ${this.props.idToken}` } })
      .then((result) => {
        this.setState({
          upvoteState: result.data.score,
          hasLoaded: true
        })
      })
  }

  render() {
    const { error, hasLoaded, contribution, upvoteState } = this.state
    if (error) {
      return (
        <Alert color="danger" className="mt-3">{error.message}</Alert>
      )
    }
    if (hasLoaded) {
      return (
        <div>
          <hr />
          <div className="clearfix mt-3">
            <div className="contribution-votes d-inline-block float-left">
              <Button color="link" onClick={() => this.updateScore(1)}>
                <Arrow classes={(upvoteState === 1 ? 'vote-up-btn vote-up-btn-active' : 'vote-up-btn')} />
              </Button>
              <div className="votes-nbr">{contribution.points}</div>
              <Button color="link" onClick={() => this.updateScore(-1)}>
                <Arrow classes={(upvoteState === -1 ? 'vote-down-btn vote-down-btn-active' : 'vote-down-btn')} downvote />
              </Button>
            </div>
            <div className="contribution-container d-inline-block float-right">
              <div className="contribution-content text-wrap">
                { contribution.text }
              </div>
              <div className="clearfix mb-3">
                <div className="contributor-infos mt-3 float-right">
                  <div className="d-inline-block mx-2 vertical-align">
                    <div className="font-average">
                      <Link to={`${routes.PROFILE}/${contribution.author._id}`} className="blue-link">
                        {contribution.author.name}
                      </Link>
                    </div>
                    <div className="font-average"><TimeAgo date={contribution.date.toString()} /></div>
                  </div>
                  <img src={user} alt="avatar" className="contribution-avatar d-inline-block  vertical-align" />
                </div>
              </div>
              <Comments
                comments={contribution.comments}
                idToken={this.props.idToken}
              />
            </div>
          </div>
        </div>
      )
    }
    return (<Spinner />)
  }
}

export default ContributionsListItem
