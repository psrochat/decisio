import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Button, Alert } from 'reactstrap'
import axios from '../../axios'
import * as routes from '../../constants/routes'
import Spinner from '../utils/Spinner'
import Arrow from './utils/Arrow'

class CommentsListItem extends React.Component {
  static propTypes = {
    comment: PropTypes.shape({
      author: PropTypes.shape({
        name: PropTypes.string.isRequired,
        _id: PropTypes.string.isRequired
      }).isRequired,
      text: PropTypes.string.isRequired,
      date: PropTypes.string.isRequired,
      points: PropTypes.number.isRequired,
      _id: PropTypes.string.isRequired
    }).isRequired,
    idToken: PropTypes.string.isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      comment: props.comment,
      error: null,
      hasLoaded: false,
      upvoteState: 0
    }
  }

  componentDidMount() {
    this.fetchUpvoteState()
  }

  updateScore = (value) => {
    axios.put(`/comment/${this.state.comment._id}/updatescore`, { value }, { headers: { Authorization: `Bearer ${this.props.idToken}` } })
      .then((response) => {
        this.setState(prevState => ({
          comment: {
            ...prevState.comment,
            points: prevState.comment.points + response.data.increment
          },
          upvoteState: response.data.score,
          hasLoaded: true
        }))
      })
      .catch(error => this.setState({
        error
      }))
  }

  fetchUpvoteState = () => {
    axios.get(`/uservote/comment/${this.state.comment._id}`, { headers: { Authorization: `Bearer ${this.props.idToken}` } })
      .then((result) => {
        this.setState({
          upvoteState: result.data.score,
          hasLoaded: true
        })
      })
  }

  render() {
    const { comment, error, hasLoaded } = this.state
    if (error) {
      return (
        <Alert color="danger" className="mt-3">{error.message}</Alert>
      )
    }
    if (hasLoaded) {
      return (
        <div key={`${comment._id}`}>
          <hr />
          <div className="clearfix my-1">
            <div className="color-rectangle d-inline" />
            <div className="comment-votes float-left d-inline">
              <Button color="link" onClick={() => this.updateScore(1)}>
                <Arrow classes={(this.state.upvoteState === 1 ? 'vote-up-btn vote-up-btn-active' : 'vote-up-btn')} />
              </Button>
              <div className="votes-nbr">{comment.points}</div>
            </div>
            <div className="comment-container d-inline-block float-right">
              <div className="comment-content text-wrap d-inline vertical-align">
                {`${comment.text} - `}
              </div>
              <Link to={`${routes.PROFILE}/${comment.author._id}`} className="font-average blue-link d-inline vertical-align">
                {comment.author.name}
              </Link>
            </div>
          </div>
        </div>
      )
    }
    return (<Spinner />)
  }
}

export default CommentsListItem
