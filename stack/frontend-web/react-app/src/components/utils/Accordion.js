import React from 'react'
import PropTypes from 'prop-types'
import { Card, CardBody, CardHeader, Collapse } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const AccordionHeader = ({ children, onClick }) => (
  <CardHeader onClick={onClick}>
    <h5 className="mb-0">
      <div className="clearfix">
        <div className="accordion-header-txt float-left d-inline vertical-align">
          { children }
        </div>
        <FontAwesomeIcon icon="angle-down" className="float-right d-inline vertical-align" />
      </div>
    </h5>
  </CardHeader>
)

AccordionHeader.propTypes = {
  children: PropTypes.string.isRequired,
  onClick: PropTypes.func
}

AccordionHeader.defaultProps = {
  onClick: null
}

const AccordionBody = ({ children, isOpen }) => (
  <Collapse isOpen={isOpen}>
    <CardBody>
      {children}
    </CardBody>
  </Collapse>
)

AccordionBody.propTypes = {
  children: PropTypes.instanceOf(Object).isRequired,
  isOpen: PropTypes.bool
}

AccordionBody.defaultProps = {
  isOpen: false
}

const AccordionItem = ({ children, isOpen, onClick }) => (
  <Card>
    {React.Children.map(children, (child) => {
      if (child.type === AccordionHeader) {
        return React.cloneElement(child, { onClick })
      }

      if (child.type === AccordionBody) {
        return React.cloneElement(child, { isOpen })
      }

      return null
    })}
  </Card>
)

AccordionItem.propTypes = {
  children: PropTypes.instanceOf(Array).isRequired,
  isOpen: PropTypes.bool,
  onClick: PropTypes.func
}

AccordionItem.defaultProps = {
  isOpen: false,
  onClick: null
}

class Accordion extends React.Component {
  static propTypes = {
    open: PropTypes.bool,
    children: PropTypes.instanceOf(Array).isRequired
  }

  static defaultProps = {
    open: false
  }

  state = {
    open: this.props.open
  }

  toggleSection = index => () => {
    this.setState(({ open }) => ({
      open: index === open ? undefined : index
    }))
  }

  render() {
    return (
      <div className="accordion">
        {React.Children.map(this.props.children, (child, index) => {
          if (child.type !== AccordionItem) return null
          return React.cloneElement(child, {
            isOpen: child.props.open || this.state.open === index,
            onClick: this.toggleSection(index)
          })
        })}
      </div>
    )
  }
}

Accordion.Item = AccordionItem
Accordion.Header = AccordionHeader
Accordion.Body = AccordionBody

export default Accordion
