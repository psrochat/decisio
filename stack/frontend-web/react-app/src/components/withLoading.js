import React from 'react'
import { withRouter } from 'react-router-dom'
import * as routes from '../constants/routes'
import AuthUserContext from './AuthUserContext'
import Spinner from './utils/Spinner'

const withLoading = (Component) => {
  const WithLoading = (props) => {
    if (props.location.pathname === routes.HOME ||
      props.location.pathname === routes.SIGN_IN ||
      props.location.pathname === routes.SIGN_UP ||
      props.location.pathname === routes.PASSWORD_FORGET) {
      return (<Component {...props} />)
    }
    return (
      <AuthUserContext.Consumer>
        { ({ collaborator }) => (collaborator ? <Component {...props} /> : <Spinner />)}
      </AuthUserContext.Consumer>
    )
  }
  return withRouter(WithLoading)
}

export default withLoading
