import React from 'react'
import PropTypes from 'prop-types'
import { Container } from 'reactstrap'
import axios from '../../axios'
import withAuthorization from '../withAuthorization'
import DecisionsListItem from './DecisionsListItem'
import PagePagination from '../utils/Pagination'
import Spinner from '../utils/Spinner'

class Decisions extends React.Component {
  static propTypes = {
    userId: PropTypes.string.isRequired,
    idToken: PropTypes.string.isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      error: null,
      hasLoaded: false,
      decisions: [],
      page: 1,
      pageSize: 5,
      hasPreviousPage: false,
      hasNextPage: false
    }
  }

  onPreviousClick = () => {
    this.setState(prevState => ({
      page: prevState.page - 1
    }), this.fetchDecisions)
  }

  onNextClick = () => {
    this.setState(prevState => ({
      page: prevState.page + 1
    }), this.fetchDecisions)
  }

  fetchDecisions = () => {
    const { props } = this
    return axios.get(
      `/decisions/user/${this.props.userId}?page=${this.state.page}&pageSize=${this.state.pageSize}&state=${props.status}`,
      { headers: { Authorization: `Bearer: ${this.props.idToken}` } }
    )
      .then(response => this.setState(prevState => ({
        decisions: response.data,
        total: response.headers.pages,
        hasPreviousPage: prevState.page > 1,
        hasNextPage: (prevState.page * prevState.pageSize) < response.headers.pages
      })))
      .then(() => {
        this.setState({
          hasLoaded: true
        })
      })
      .catch(error => this.setState({
        error
      }))
  }

  componentDidMount = () => {
    this.fetchDecisions()
  }

  render() {
    const { error, hasLoaded, decisions, hasPreviousPage, hasNextPage } = this.state
    if (error) {
      return (
        <div>Error: {error.message}</div>
      )
    }
    if (hasLoaded) {
      return (
        <Container>
          <div>{ decisions.map(decision => (
            <DecisionsListItem
              decision={decision}
              idToken={this.props.idToken}
              key={decision._id}
            />
          )) }
            <PagePagination
              onPreviousClick={this.onPreviousClick}
              onNextClick={this.onNextClick}
              hasPreviousPage={hasPreviousPage}
              hasNextPage={hasNextPage}
            />
          </div>
        </Container>
      )
    }
    return (
      <Spinner />
    )
  }
}

const authCondition = authUser => !!authUser

export default withAuthorization(authCondition)(Decisions)
