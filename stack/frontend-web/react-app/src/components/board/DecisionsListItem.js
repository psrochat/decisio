import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Row, Col, Badge } from 'reactstrap'
import TimeAgo from 'react-timeago'
import axios from '../../axios'

class DecisionsListItem extends React.Component {
  static propTypes = {
    idToken: PropTypes.string.isRequired,
    decision: PropTypes.shape({
      _id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      author: PropTypes.shape({
        name: PropTypes.string.isRequired,
        _id: PropTypes.string.isRequired
      }).isRequired,
      views: PropTypes.number.isRequired,
      lastUpdate: PropTypes.string.isRequired,
      date: PropTypes.string.isRequired,
      state: PropTypes.string.isRequired,
      teams: PropTypes.arrayOf(
        PropTypes.shape({
          name: PropTypes.string.isRequired
        }).isRequired
      ).isRequired,
      options: PropTypes.arrayOf(
        PropTypes.shape({
          _id: PropTypes.string.isRequired,
          title: PropTypes.string.isRequired,
          description: PropTypes.string.isRequired,
          votes: PropTypes.number.isRequired
        }).isRequired
      ).isRequired
    }).isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      count: 0
    }
  }

  componentDidMount() {
    this.countContributions()
  }

  countVotes = () => this.props.decision.options.reduce((acc, current) => acc + current.votes, 0)

  countContributions = () => axios.get(`/contributions/decision/${this.props.decision._id}/count/`, { headers: { Authorization: `Bearer ${this.props.idToken}` } })
    .then(response => (
      this.setState({
        count: response.data.count
      })))
    .catch((error) => {
      console.error(error.message)
    })

  render() {
    const { decision } = this.props

    return (
      <div className="list-item">
        <div className="px-2">
          <Row className="py-2">
            <Col className="info-boxes-col pr-2" lg="auto" md="auto">
              <div>{/* TODO make responsive with breakpoints */}
                <div className="info-box d-inline-block mr-2 info-border-green">
                  <div className="info-value d-block">{ decision.views }</div>
                  <div className="info-title d-block">views</div>
                </div>
                <div className="info-box d-inline-block mr-2 info-border-green">
                  <div className="info-value d-block">{ this.countVotes() }</div>
                  <div className="info-title d-block">votes</div>
                </div>
                <div className="info-box d-inline-block info-border-green">
                  <div className="info-value d-block">{ this.state.count }</div>
                  <div className="info-title d-block">contrib.</div>
                </div>
              </div>
            </Col>
            <Col className="subject-col pl-2">
              <Link to={{ pathname: `/decision/${decision._id}` }} className="subject-title">
                {decision.title}
              </Link>
              <div className="clearfix">
                <span className="float-left">
                  <span className="subject-subtitle mr-2">
                  #13 &middot; opened <TimeAgo date={decision.date.toString()} />&nbsp;
                  by {decision.author.name}
                  </span>
                  <div className="d-inline-block">
                    {
                    decision.tags.map(tag => (
                      <Badge key={tag._id} className="mr-2" color={tag.color}>{tag.title}</Badge>
                    ))
                  }
                  </div>
                </span>
                <span className="subject-timestamp float-right">
                Updated <TimeAgo date={decision.lastUpdate} />
                </span>
              </div>
            </Col>
          </Row>
        </div>
        <hr />
      </div>
    )
  }
}

export default DecisionsListItem
